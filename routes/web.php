<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('zerotoone.site')->group(function() {
	Route::get('/', function() {
		return view('welcome');
	});
});

Route::domain('api.zerotoone.site')->group(function() {
	Route::prefix('v1')->group(function() {
		Route::get('auth/check', 'Api\\v1\\AuthController@check');
		Route::post('auth/login', 'Api\\v1\\Auth\\LoginController@login');
		Route::middleware('auth:api')->group(function() {
			Route::get('contacts', 'Api\\v1\\ContactsController@all');
			Route::post('contacts', 'Api\\v1\\ContactsController@add');
			Route::get('contacts/{id}', 'Api\\v1\\ContactsController@get');
			Route::delete('contacts/{id}', 'Api\\v1\\ContactsController@delete');
			Route::put('contacts/{id}', 'Api\\v1\\ContactsController@update');

			Route::get('user', 'Api\\v1\\UsersController@get');
			Route::put('user/profile', 'Api\\v1\\UsersController@updateProfile');
		});
	});
});