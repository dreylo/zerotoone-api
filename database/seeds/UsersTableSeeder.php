<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		User::create([
			'name' => 'Admin user',
			'email' => 'admin@app.com',
			'password' => bcrypt('password'),
			'organization' => 'App'
		]);
	}
}