<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{

	/**
	* @var 		$table
	* @access 	protected
	*/
	protected 	$table = 'oauth_clients';

}