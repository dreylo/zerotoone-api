<?php

namespace App;

use App\{Contact, Notification};
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable, HasApiTokens;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'organization'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'created_at' => 'datetime'
	];

	/**
	* Returns user contacts model.
	*
	* @access 	public
	* @return 	Object
	*/
	public function contacts()
	{
		return $this->hasMany(Contact::class);
	}

	/**
	* Returns user notifications model.
	*
	* @access 	public
	* @return 	Object
	*/
	public function notifications()
	{
		return $this->hasMany(Notification::class);
	}

}