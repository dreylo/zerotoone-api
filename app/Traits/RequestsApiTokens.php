<?php

namespace App\Traits;

use App\OauthClient;
use Illuminate\Http\Request;

trait RequestsApiTokens
{

	/**
	* Issues and returns access_token, refresh_token, bearer and expiry time.
	*
	* @param 	$request
	* @access 	public
	* @return 	String
	*/
	public function requestTokens($request)
	{
		$client = OauthClient::where('password_client', 1)->first();
		$requestParams = [
			'grant_type' => 'password',
			'client_id' => $client->id,
			'client_secret' => $client->secret,
			'username' => $request->input('email'),
			'password' => $request->input('password')
		];

		return app()->handle(Request::create('oauth/token', 'POST', $requestParams));
	}

}