<?php

namespace App\Http\Controllers\Api\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
	
	/**
	* Returns logged in user.
	*
	* @access 	public
	* @return 	response
	*/
	public function get()
	{
		return response()->json(auth()->user());
	}

	/**
	* Updates a logged in user.
	*
	* @param 	$request
	* @access 	public
	* @return 	response
	*/
	public function updateProfile(Request $request)
	{
		$user = auth()->user();
		$validation = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email|unique:users,email,' . $user->id
		]);

		if ($validation->fails()) {
			return response()->json($validation->errors()->first(), 400);
		}

		$user->update([
			'name' => $request->name,
			'email' => $request->email,
			'organization' => $request->organization
		]);
	}

}