<?php

namespace App\Http\Controllers\Api\v1;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{

	/**
	* Returns a user's contacts.
	*
	* @param 	$request
	* @access 	public
	* @return 	response
	*/
	public function all(Request $request)
	{
		return response()->json(auth()->user()->contacts()->get());
	}

	/**
	* Creates a new contact.
	*
	* @param 	$request
	* @access 	public
	* @return 	response
	*/
	public function add(Request $request)
	{
		$validation = $request->validate([
			'name' => 'required',
			'phone_number' => 'required|integer'
		]);

		auth()->user()
		->contacts()
		->create([
			'name' => $request->name,
			'phone' => $request->phone_number
		]);
	}

	/**
	* Updates an existing contact.
	*
	* @param 	$request
	* @access 	public
	* @return 	response
	*/
	public function update(Request $request, $id)
	{
		$validation = $request->validate([
			'name' => 'required',
			'phone' => 'required|integer'
		]);

		$contact = auth()->user()->contacts()->find($id);
		$contact->update([
			'name' => $request->name,
			'phone' => $request->phone
		]);
	}

	/**
	* Deletes a user's contact.
	*
	* @param 	$request
	* @access 	public
	* @return 	response
	*/
	public function delete(Request $request, $id)
	{
		auth()->user()
		->contacts()
		->delete($id);
	}

	/**
	* Retrieves a user's contact.
	*
	* @param 	$request
	* @access 	public
	* @return 	response
	*/
	public function get(Request $request, $id)
	{
		return response()->json(auth()->user()->contacts()->find($id));
	}

}