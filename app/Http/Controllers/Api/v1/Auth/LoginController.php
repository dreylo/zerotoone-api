<?php

namespace App\Http\Controllers\Api\v1\Auth;

use Illuminate\Http\Request;
use App\Traits\RequestsApiTokens;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{

	use RequestsApiTokens;

	/**
	* Generates and returns token for a user.
	*
	* @param 	$request
	* @access 	public
	* @return 	String
	*/
	public function login(Request $request)
	{
		$response = $this->requestTokens($request);
		if ($response->getStatusCode() != 200) {
			return response()->json([], 403);
		}

		return $response;
	}

}