<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
	
	/**
	* Checks if a user is authenticated or not.
	*
	* @access 	public
	* @return 	response
	*/
	public function check()
	{
		if (auth()->guard('api')->user()) {
			return response()->json([]);
		}

		return response()->json([], 403);
	}

}